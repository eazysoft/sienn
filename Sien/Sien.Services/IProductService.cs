﻿using Microsoft.EntityFrameworkCore;
using Sienn.Data.Infrastructure;
using Sienn.Data.Models;
using Sienn.Data.Repositories;
using Sienn.Services.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Sienn.Services
{
    public interface IProductService
    {
        Product GetProduct(Guid id);
        Task<Product> GetProductAsync(Guid id);
        IPagedList<Product> List(bool? isAvailable, int? productTypeId, int? categoryId, int pageIndex, int pageSize);
        IPagedList<ProductViewModel> ListView(bool? isAvailable, int? productTypeId, int? categoryId, int pageIndex, int pageSize);

        void Add(ProductUpdateViewModel product);
        void Delete(Product product);
        void Update(ProductUpdateViewModel product);
        bool Exists(Guid id);
    }

    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public void Add(ProductUpdateViewModel product)
        {
            var newProduct = new Product();
            _productRepository.Add(product.Update(newProduct));
        }

        public void Delete(Product product)
        {
            _productRepository.Delete(product);
        }

        public bool Exists(Guid id)
        {
            return _productRepository.Query().Any(p => p.Id == id);
        }

        public Product GetProduct(Guid id)
        {
            return _productRepository.Get(id);
        }

        public async Task<Product> GetProductAsync(Guid id)
        {
            var product = await _productRepository.GetProductAsync(id);
            return product;
        }

        public IPagedList<Product> List(bool? isAvailable, int? productTypeId, int? categoryId, int pageIndex, int pageSize)
        {
            var pageProducts = _productRepository.FetchPaged(q => q.
                Where(e => (!isAvailable.HasValue || e.IsAvailable == isAvailable)
                                && (!productTypeId.HasValue || productTypeId.Value == e.TypeId) 
                                && (!categoryId.HasValue || e.ProductsInCategories.Any(p => p.CategoryId == categoryId.Value)))
                .OrderBy(t => t.Name), pageIndex, pageSize);

            return pageProducts;
        }

        public IPagedList<ProductViewModel> ListView(bool? isAvailable, int? productTypeId, int? categoryId, int pageIndex, int pageSize)
        {
            var pageProducts = _productRepository.FetchPaged(q => q.Include(p => p.Unit)
                                                                   .Include(p => p.Type)
                                                                   .Include(p => p.ProductsInCategories)
                                .Where(e => (!isAvailable.HasValue || e.IsAvailable == isAvailable)
                                && (!productTypeId.HasValue || productTypeId.Value == e.TypeId)
                                && (!categoryId.HasValue || e.ProductsInCategories.Any(p => p.CategoryId == categoryId.Value)))
                .OrderBy(t => t.Name), pageIndex, pageSize);
            
            return new PagedList<ProductViewModel>(pageProducts.Select(p => new ProductViewModel(p)), pageIndex, pageSize, pageProducts.TotalCount);
        }

        public void Update(ProductUpdateViewModel product)
        {
            var productToUpdate = _productRepository.Get(product.Id);
            if (productToUpdate != null)
            {
                _productRepository.Update(product.Update(productToUpdate));
            }
        }
    }
}
