﻿using Sienn.Data.Infrastructure;
using Sienn.Data.Models;
using Sienn.Data.Repositories;
using Sienn.Services.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace Sienn.Services
{
    public interface IProductTypeService
    {
        Task<ProductTypeViewModel> GetProductTypeAsync(int id);
        IPagedList<ProductTypeViewModel> List(int pageIndex, int pageSize);

        void Add(ProductTypeViewModel productType);
        void Delete(int id);
        void Update(ProductTypeViewModel productType);
        bool Exists(int id);
    }

    public class ProductTypeService : IProductTypeService
    {
        private readonly IProductTypeRepository _productTypeRepository;

        public ProductTypeService(IProductTypeRepository productTypeRepository)
        {
            _productTypeRepository = productTypeRepository;
        }

        public void Add(ProductTypeViewModel productType)
        {
            var newProductType = new ProductType();

            newProductType.Code = productType.Code;
            newProductType.Description = productType.Description;

            _productTypeRepository.Add(newProductType);
        }

        public void Delete(int id)
        {
            var productType = _productTypeRepository.Get(id);
            _productTypeRepository.Delete(productType);
        }

        public bool Exists(int id)
        {
            return _productTypeRepository.Query().Any(p => p.Id == id);
        }
        
        public async Task<ProductTypeViewModel> GetProductTypeAsync(int id)
        {
            var productType = await _productTypeRepository.GetAsync(id);
            return new ProductTypeViewModel(productType);
        }
        
        public IPagedList<ProductTypeViewModel> List(int pageIndex, int pageSize)
        {
            var ProductTypesCategories = _productTypeRepository.FetchPaged(q => q.OrderBy(t => t.Code), pageIndex, pageSize);
            return new PagedList<ProductTypeViewModel>(ProductTypesCategories.Select(p => new ProductTypeViewModel(p)), pageIndex, pageSize, ProductTypesCategories.TotalCount);
        }

        public void Update(ProductTypeViewModel productType)
        {
            var ProductTypeToUpdate = _productTypeRepository.Get(productType.Id);
            if (ProductTypeToUpdate != null)
            {
                ProductTypeToUpdate.Code = productType.Code;
                ProductTypeToUpdate.Description = productType.Description;

                _productTypeRepository.Update(ProductTypeToUpdate);
            }
        }
    }
}
