﻿using Sienn.Data.Models;

namespace Sienn.Services.ViewModels
{
    public class UnitViewModel
    {
        public UnitViewModel()
        {

        }
        public UnitViewModel(Unit unit)
        {
            Id = unit.Id;
            Code = unit.Code;
            Description = unit.Description;
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
