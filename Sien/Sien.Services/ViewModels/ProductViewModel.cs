﻿using Sienn.Data.Models;
using System;
using System.Globalization;

namespace Sienn.Services.ViewModels
{
    public class ProductViewModel
    {
        public ProductViewModel()
        {

        }

        public ProductViewModel(Product product)
        {
            if (product != null)
            {
                Id = product.Id;
                ProductDescription = product.Description;
                Price = product.UnitPrice.ToString("C2", CultureInfo.CreateSpecificCulture("pl-PL"));
                IsAvailable = product.IsAvailable ? "Available" : "Unavailable";
                DeliveryDate = product.DeliveryDate;
                CategoriesCount = product.ProductsInCategories.Count;
                Unit = $"({product.Unit.Code}) {product.Unit.Description}";
                Type = $"({product.Type.Code}) {product.Type.Description}";
            }
        }

        public Guid Id { get; set; }
        public string ProductDescription { get; set; }
        public string Price { get; set; }
        public string IsAvailable { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int CategoriesCount { get; set; }
        public string Unit { get; set; }
        public string Type { get; set; }
    }
}
