﻿using Sienn.Data.Models;

namespace Sienn.Services.ViewModels
{
    public class ProductTypeViewModel
    {
        public ProductTypeViewModel()
        {

        }

        public ProductTypeViewModel(ProductType productType)
        {
            Id = productType.Id;
            Code = productType.Code;
            Description = productType.Description;
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
