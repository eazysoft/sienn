﻿using Sienn.Data.Models;
using System;

namespace Sienn.Services.ViewModels
{
    public class ProductUpdateViewModel
    {
        public ProductUpdateViewModel()
        {
            //Categories = new List<int>();
        }
        public Guid Id { get; set; }
        public int UnitId { get; set; }
        public int TypeId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public bool IsAvailable { get; set; }
        public DateTime DeliveryDate { get; set; }

        //public List<int> Categories { get; set; }


        public Product Update(Product product)
        {
            product.UnitId = UnitId;
            product.TypeId = TypeId;
            product.Code = Code;
            product.Name = Name;
            product.Description = Description;
            product.UnitPrice = UnitPrice;
            product.IsAvailable = IsAvailable;
            product.DeliveryDate = DeliveryDate;
            return product;
        }
    }
}
