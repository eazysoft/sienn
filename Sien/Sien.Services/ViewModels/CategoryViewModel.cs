﻿using Sienn.Data.Models;

namespace Sienn.Services.ViewModels
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {

        }

        public CategoryViewModel(Category category)
        {
            Id = category.Id;
            Code = category.Code;
            Description = category.Description;
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
