﻿using Sienn.Data.Infrastructure;
using Sienn.Data.Models;
using Sienn.Data.Repositories;
using Sienn.Services.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace Sienn.Services
{
    public interface ICategoryService
    {
        Task<CategoryViewModel> GetCategoryAsync(int id);
        IPagedList<CategoryViewModel> List(int pageIndex, int pageSize);

        void Add(CategoryViewModel product);
        void Delete(int id);
        void Update(CategoryViewModel product);
        bool Exists(int id);
    }

    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public void Add(CategoryViewModel category)
        {
            var newCategory = new Category();

            newCategory.Code = category.Code;
            newCategory.Description = category.Description;

            _categoryRepository.Add(newCategory);
        }

        public void Delete(int id)
        {
            var category = _categoryRepository.Get(id);
            _categoryRepository.Delete(category);
        }

        public bool Exists(int id)
        {
            return _categoryRepository.Query().Any(p => p.Id == id);
        }
        
        public async Task<CategoryViewModel> GetCategoryAsync(int id)
        {
            var category = await _categoryRepository.GetAsync(id);
            return new CategoryViewModel(category);
        }
        
        public IPagedList<CategoryViewModel> List(int pageIndex, int pageSize)
        {
            var pageCategories = _categoryRepository.FetchPaged(q => q.OrderBy(t => t.Code), pageIndex, pageSize);
            return new PagedList<CategoryViewModel>(pageCategories.Select(p => new CategoryViewModel(p)), pageIndex, pageSize, pageCategories.TotalCount);
        }

        public void Update(CategoryViewModel category)
        {
            var categoryToUpdate = _categoryRepository.Get(category.Id);
            if (categoryToUpdate != null)
            {
                categoryToUpdate.Code = category.Code;
                categoryToUpdate.Description = category.Description;

                _categoryRepository.Update(categoryToUpdate);
            }
        }
    }
}
