﻿using Sienn.Data.Infrastructure;
using Sienn.Data.Models;
using Sienn.Data.Repositories;
using Sienn.Services.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace Sienn.Services
{
    public interface IUnitService
    {
        Task<UnitViewModel> GetUnitAsync(int id);
        IPagedList<UnitViewModel> List(int pageIndex, int pageSize);

        void Add(UnitViewModel unit);
        void Delete(int id);
        void Update(UnitViewModel unit);
        bool Exists(int id);
    }

    public class UnitService : IUnitService
    {
        private readonly IUnitRepository _unitRepository;

        public UnitService(IUnitRepository unitRepository)
        {
            _unitRepository = unitRepository;
        }

        public void Add(UnitViewModel unit)
        {
            var newUnit = new Unit();

            newUnit.Code = unit.Code;
            newUnit.Description = unit.Description;

            _unitRepository.Add(newUnit);
        }

        public void Delete(int id)
        {
            var unit = _unitRepository.Get(id);
            _unitRepository.Delete(unit);
        }

        public bool Exists(int id)
        {
            return _unitRepository.Query().Any(p => p.Id == id);
        }
        
        public async Task<UnitViewModel> GetUnitAsync(int id)
        {
            var unit = await _unitRepository.GetAsync(id);
            return new UnitViewModel(unit);
        }
        
        public IPagedList<UnitViewModel> List(int pageIndex, int pageSize)
        {
            var pageUnits = _unitRepository.FetchPaged(q => q.OrderBy(t => t.Code), pageIndex, pageSize);
            return new PagedList<UnitViewModel>(pageUnits.Select(p => new UnitViewModel(p)), pageIndex, pageSize, pageUnits.TotalCount);
        }

        public void Update(UnitViewModel unit)
        {
            var unitToUpdate = _unitRepository.Get(unit.Id);
            if (unitToUpdate != null)
            {
                unitToUpdate.Code = unit.Code;
                unitToUpdate.Description = unit.Description;

                _unitRepository.Update(unitToUpdate);
            }
        }
    }
}
