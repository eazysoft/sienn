﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sienn.Services;
using Sienn.Services.ViewModels;

namespace Sienn.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Units")]
    public class UnitsController : Controller
    {
        private readonly IUnitService _unitService;

        public UnitsController(IUnitService unitService)
        {
            _unitService = unitService;
        }

        // GET: api/Units
        [HttpGet]
        public IEnumerable<UnitViewModel> GetUnits(int pageIndex = 0, int pageSize = 10)
        {
            return _unitService.List(pageIndex, pageSize);
        }

        // GET: api/Units/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUnit([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var unit = await _unitService.GetUnitAsync(id);

            if (unit == null)
            {
                return NotFound();
            }

            return Ok(unit);
        }

        // PUT: api/Units/5
        [HttpPut("{id}")]
        public IActionResult PutUnit([FromRoute] int id, [FromBody] UnitViewModel unit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != unit.Id)
            {
                return BadRequest();
            }

            try
            {
                _unitService.Update(unit);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UnitExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Units
        [HttpPost]
        public async Task<IActionResult> PostUnit([FromBody] UnitViewModel unit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _unitService.Add(unit);

            return CreatedAtAction("GetUnit", new { id = unit.Id }, unit);
        }

        // DELETE: api/Units/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUnit([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var unit = await _unitService.GetUnitAsync(id);
            if (unit == null)
            {
                return NotFound();
            }

            _unitService.Delete(unit.Id);

            return Ok(unit);
        }

        private bool UnitExists(int id)
        {
            return _unitService.Exists(id);
        }
    }
}