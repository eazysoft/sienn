﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sienn.Services;
using Sienn.Services.ViewModels;

namespace Sienn.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/ProductTypes")]
    public class ProductTypesController : Controller
    {
        private readonly IProductTypeService _productTypeService;

        public ProductTypesController(IProductTypeService productTypeService)
        {
            _productTypeService = productTypeService;
        }

        // GET: api/ProductTypes
        [HttpGet]
        public IEnumerable<ProductTypeViewModel> GetTypes(int pageIndex = 0, int pageSize = 10)
        {
            return _productTypeService.List(pageIndex, pageSize);
        }

        // GET: api/ProductTypes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductType([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productType = await _productTypeService.GetProductTypeAsync(id);

            if (productType == null)
            {
                return NotFound();
            }

            return Ok(productType);
        }

        // PUT: api/ProductTypes/5
        [HttpPut("{id}")]
        public IActionResult PutProductType([FromRoute] int id, [FromBody] ProductTypeViewModel productType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productType.Id)
            {
                return BadRequest();
            }
            
            try
            {
                _productTypeService.Update(productType);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProductTypes
        [HttpPost]
        public IActionResult PostProductType([FromBody] ProductTypeViewModel productType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _productTypeService.Add(productType);

            return CreatedAtAction("GetProductType", new { id = productType.Id }, productType);
        }

        // DELETE: api/ProductTypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductType([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productType = await _productTypeService.GetProductTypeAsync(id);
            if (productType == null)
            {
                return NotFound();
            }

            _productTypeService.Delete(productType.Id);

            return Ok(productType);
        }

        private bool ProductTypeExists(int id)
        {
            return _productTypeService.Exists(id);
        }
    }
}