﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Sienn.Data.Infrastructure
{
    public interface IRepository<TId, TEntity> where TEntity : IEntity<TId>
    {
        TEntity Get(TId id);
        Task<TEntity> GetAsync(TId id);
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);

        IEnumerable<TEntity> GetRange(IEnumerable<TId> ids);
        IEnumerable<TEntity> GetRange(int start, int count);
        IEnumerable<TEntity> GetRange(int start, int count, Expression<Func<TEntity, bool>> predicate);


        IPagedList<TEntity> FetchPaged(Func<IQueryable<TEntity>, IQueryable<TEntity>> query, int pageIndex, int pageSize);
        IQueryable<TEntity> Query();
    }

    public interface IRepository<TEntity> : IRepository<Guid, TEntity> where TEntity : IEntity<Guid>
    {

    }
}
