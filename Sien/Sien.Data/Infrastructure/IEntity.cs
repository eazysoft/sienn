﻿namespace Sienn.Data.Infrastructure
{
    public interface IEntity<TId>
    {
        TId Id { get; }
    }
}
