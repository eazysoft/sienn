﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Sienn.Data.Infrastructure
{
    public class SiennRepository<TEntity> : SiennRepository<Guid, TEntity> where TEntity : Entity
    {
        public SiennRepository(SiennContext dbContext) : base(dbContext)
        {
        }
    }

    public class SiennRepository<TId, TEntity> : IRepository<TId, TEntity> where TEntity : Entity<TId>
    {
        protected readonly SiennContext _dbContext;

        public SiennRepository(SiennContext dbContext)
        {
            _dbContext = dbContext;
        }

        public TEntity Get(TId id)
        {
            return _dbContext.Set<TEntity>().Find(id);
        }

        public async Task<TEntity> GetAsync(TId id)
        {
            return await _dbContext.Set<TEntity>().FindAsync(id);
        }

        public void Add(TEntity entity)
        {
            _dbContext.Set<TEntity>().Add(entity);
            SaveChanges();
        }

        public void Update(TEntity entity)
        {
            _dbContext.Set<TEntity>().Update(entity);
            SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            var local = _dbContext.Set<TEntity>()
                         .Local
                         .FirstOrDefault(f => f.Id.Equals(entity.Id));
            if (local != null)
            {
                _dbContext.Entry(local).State = EntityState.Detached;
            }
            _dbContext.Entry(entity).State = EntityState.Modified;

            _dbContext.Set<TEntity>().Attach(entity);
            _dbContext.Set<TEntity>().Remove(entity);

            SaveChanges();
        }

        public IEnumerable<TEntity> GetRange(IEnumerable<TId> ids)
        {
            return _dbContext.Set<TEntity>().AsQueryable().Where(i => ids.Contains(i.Id));
        }

        public IEnumerable<TEntity> GetRange(int start, int count)
        {
            return _dbContext.Set<TEntity>().AsQueryable().Skip(start).Take(count);
        }

        public IEnumerable<TEntity> GetRange(int start, int count, Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Set<TEntity>().AsQueryable().Where(predicate).Skip(start).Take(count);
        }
        
        public IPagedList<TEntity> FetchPaged(Func<IQueryable<TEntity>, IQueryable<TEntity>> query, int pageIndex, int pageSize)
        {
            return FetchPagedResults(query(Query()), pageIndex, pageSize);
        }

        public IQueryable<TEntity> Query()
        {
            return _dbContext.Set<TEntity>().AsQueryable();
        }

        private IPagedList<TEntity> FetchPagedResults(IQueryable<TEntity> query, int pageIndex, int pageSize)
        {
            var futureCount = query.Count();

            var isOrdered = query.Expression.Type == typeof(IOrderedQueryable<TEntity>);
            if (!isOrdered)
                query = query.OrderBy(x => x.Id);

            return new PagedList<TEntity>(query.Skip(pageSize * pageIndex).Take(pageSize),
                pageIndex, pageSize, futureCount);
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}
