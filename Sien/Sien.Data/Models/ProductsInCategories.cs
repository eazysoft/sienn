﻿using System;

namespace Sienn.Data.Models
{
    public partial class ProductsInCategories
    {
        public Guid ProductId { get; set; }
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }
        public virtual Product Product { get; set; }
    }
}
