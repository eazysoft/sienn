﻿using Sienn.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sienn.Data.Models
{
    [Table("Units")]
    public partial class Unit : Entity<int>
    {
        public Unit()
        {
            Products = new HashSet<Product>();
        }
        
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
