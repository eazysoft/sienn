﻿using Sienn.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sienn.Data.Models
{
    [Table("Categories")]
    public partial class Category : Entity<int>
    {
        public Category()
        {
            ProductsInCategories = new HashSet<ProductsInCategories>();
        }
        
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }

        public virtual ICollection<ProductsInCategories> ProductsInCategories { get; set; }
    }
}
