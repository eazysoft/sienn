﻿using Sienn.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sienn.Data.Models
{
    [Table("Products")]
    public partial class Product : Entity
    {
        public Product()
        {
            ProductsInCategories = new HashSet<ProductsInCategories>();
        }
        
        public int UnitId { get; set; }
        public int TypeId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public bool IsAvailable { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime Created { get; set; }

        public virtual ProductType Type { get; set; }
        public virtual Unit Unit { get; set; }
        public virtual ICollection<ProductsInCategories> ProductsInCategories { get; set; }
    }
}
