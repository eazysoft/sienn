﻿using Sienn.Data.Infrastructure;
using Sienn.Data.Models;

namespace Sienn.Data.Repositories
{
    public class ProductTypeRepository : SiennRepository<int, ProductType>, IProductTypeRepository
    {
        public ProductTypeRepository(SiennContext dbContext)
            : base(dbContext)
        {

        }
    }

    public interface IProductTypeRepository : IRepository<int, ProductType>
    {
    }
}
