﻿using Sienn.Data.Infrastructure;
using Sienn.Data.Models;

namespace Sienn.Data.Repositories
{
    public class CategoryRepository : SiennRepository<int, Category>, ICategoryRepository
    {
        public CategoryRepository(SiennContext dbContext)
            : base(dbContext)
        {

        }
    }

    public interface ICategoryRepository : IRepository<int, Category>
    {
    }
}
