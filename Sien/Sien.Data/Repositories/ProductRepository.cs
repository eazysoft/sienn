﻿using Microsoft.EntityFrameworkCore;
using Sienn.Data.Infrastructure;
using Sienn.Data.Models;
using System;
using System.Threading.Tasks;

namespace Sienn.Data.Repositories
{
    public class ProductRepository : SiennRepository<Product>, IProductRepository
    {
        public ProductRepository(SiennContext dbContext)
            : base(dbContext)
        {

        }

        public async Task<Product> GetProductAsync(Guid id)
        {
            return await _dbContext.Products.Include(p => p.Unit)
                                      .Include(p => p.Type)
                                      .Include(p => p.ProductsInCategories)
                                      .FirstOrDefaultAsync(p => p.Id == id);
        }
    }

    public interface IProductRepository : IRepository<Product>
    {
        Task<Product> GetProductAsync(Guid id);
    }
}
