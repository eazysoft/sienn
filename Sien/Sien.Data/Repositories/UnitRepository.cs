﻿using Sienn.Data.Infrastructure;
using Sienn.Data.Models;

namespace Sienn.Data.Repositories
{
    public class UnitRepository : SiennRepository<int, Unit>, IUnitRepository
    {
        public UnitRepository(SiennContext dbContext)
            : base(dbContext)
        {

        }
    }

    public interface IUnitRepository : IRepository<int, Unit>
    {
    }
}
