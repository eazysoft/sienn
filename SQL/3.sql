SELECT TOP 3 cat.*
FROM dbo.Categories as cat
LEFT JOIN (SELECT CategoryId, count(*) productsCount, AVG(prod.[UnitPrice]) avgPrice
           FROM dbo.ProductsInCategories 
		   left join dbo.Products as prod on prod.Id = ProductId
           GROUP BY CategoryId) as prc ON prc.CategoryId = cat.Id
Order by avgPrice desc