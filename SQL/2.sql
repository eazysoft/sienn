SELECT prod.*
FROM dbo.Products as prod
LEFT JOIN (SELECT ProductId, count(*) c 
           FROM dbo.ProductsInCategories 
           GROUP BY ProductId) as prc ON prc.ProductId = prod.Id
WHERE COALESCE(c, 0) > 1 AND prod.IsAvailable = 1